import React from 'react';
import AddFishForm from './AddFishForm';
import EditFishForm from './EditFishForm';
import PropTypes from 'prop-types';
import Login from './Login';
import firebase from 'firebase';
import base, { firebaseApp } from '../base';

export default class Inventory extends React.Component {
    
    static propTypes = {
        addFish: PropTypes.func,
        updateFish: PropTypes.func,
        deleteFish: PropTypes.func,
        loadSampleFishes: PropTypes.func,
        fishes: PropTypes.object
    }

    state = {
        uid: null,
        owner: null
    };
    
    componentDidMount() {
        firebase.auth().onAuthStateChanged(user => {
            if(user) {
                this.authHandler({ user })
            }
        });
    }
    
    renderEditForm = key => {
        return (
            <EditFishForm 
                key={key} 
                index={key} 
                updateFish={this.props.updateFish} 
                deleteFish={this.props.deleteFish}
                fish={this.props.fishes[key]} />
        );
    }
    
    authHandler = async authData => {
        // 1. Look up the current store in the firebase
        const store = await base.fetch(this.props.storeId, { context: this });
        // 2. claim it there is no owner
        if(!store.owner) {
            // save it as our own
            await base.post(`${this.props.storeId}/owner`, {
                data: authData.user.uid
            });
        }
        // 3.  Set the state of the inventory component to reflect the current user
        this.setState({
            uid: authData.user.uid,
            owner: store.owner || authData.user.uid
        });
    }
    
    authenticate = provider => {
        const authProvider = new firebase.auth[`${provider}AuthProvider`]();
        firebaseApp.auth().signInWithPopup(authProvider)
            .then(this.authHandler);
    }
    
    logout = async () => {
        await firebase.auth().signOut();
        this.setState({uid: null})
    }
    
    render() {
        const logout = <button onClick={this.logout}>Log Out!</button>
        // 1. Check if tjer are logged in
        if(!this.state.uid) {
            return <Login authenticate={this.authenticate} />
        }
        
        // 2. Check if ther are not the owner of the store
        if(this.state.uid !== this.state.owner) {
            return (
                <div>
                    <p>Sorry, yout are not the owner</p>
                    {logout}
                </div>
            );
        }
        const fishIds = Object.keys(this.props.fishes);
        return (
            <div className="inventory">
                <h2>Inventory</h2>
                {logout}
                {fishIds.map(this.renderEditForm)}
                <AddFishForm addFish={this.props.addFish} />
                <button onClick={this.props.loadSampleFishes}>Load Sample Fishes</button>
            </div>
        );
    }
}