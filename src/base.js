import Rebase from 're-base';
import firebase from 'firebase';

const firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyAIx34Q4OnFwYcgIIIRFgwjuH9gDHNs_5A",
    authDomain: "jealous-thoughtless-syllabuses.firebaseapp.com",
    databaseURL: "https://jealous-thoughtless-syllabuses.firebaseio.com"
});

const base = Rebase.createClass(firebaseApp.database());

export { firebaseApp }

export default base;